(function($) {
	$(document).ready(function () {
		if ($("#site-sidebar").css("display") == "none" ) {
			$("#site-sidebar").addClass("no-content");
		}
		
		enableMobile();

		var timer = window.setTimeout(function() {}, 0);
   
		$(window).on('resize', function() {
			window.clearTimeout(timer);
			timer = window.setTimeout(function() {	
			enableMobile();
			}, 500);//Set a timeout to get the resize-end event. http://forum.jquery.com/topic/the-resizeend-event.
  	
		}); 
   });

	function enableMobile () {
		var cWinSize = $(window).width();
		if (cWinSize < 768) {
			
			$("html>body>#site>#main").css({"width": "100%", "float":"none"});
			$("#watermark, #skip, #uw-header>.global-menu, #uw-header #uw-search-label").hide();
			$(".global-menu>li").css("float","none");
			$("#site-footer, #uw-footer").css("font-size", "100%");
			
			$("#site-navigation").addClass("mob-pan left-bar");
			$("#site-sidebar").addClass("mob-pan right-bar");
			$("#site-sidebar").css("left", cWinSize-$("#site-sidebar").width());
			$(".mob-pan").width(cWinSize*0.4);

			var cWinHeight = $("#site").height();
			$(".mob-pan").height(cWinHeight);
			$("#content").width(cWinSize -20);

			if ($("#mob-menu-btn").length){
				//Do nothing because we only need one button.
			} else {
				$("#uw-header>.global-menu").before("<div id='mob-menu-row' class='full-row'><a id='mob-menu-btn'><span class='bars'> </span><span class='bars'> </span><span class='bars'> </span></a></div>");
			}
		
			if ($("#mob-header").length){
				//Do nothing because we only need one button.
			} else {
				$("#site-header").after("<div id='mob-header'><a class='bar-btn left' id='left-bar-btn'> &gt;&gt;</a><a class='bar-btn right' id='right-bar-btn'>&lt;&lt;</a></div>");
			}

			if ($("#site-sidebar").hasClass("no-content")){
				$("#right-bar-btn").hide();
			} else {
				$("#right-bar-btn").show();
			}
			
			closeBar("#site-navigation", 600);
			closeBar("#site-sidebar", 600);


			
			$("#mob-menu-btn").click(function(){
				if ($("#uw-header > .global-menu").hasClass("sliding")){
					//Do nothing because the menu is sliding
				} else{
					$("#uw-header > .global-menu").slideToggle(400, function(){
						$("#uw-header > .global-menu").removeClass("sliding");
					});
					
				}
			});
			
			$(".bar-btn").click(function(event) {

				event.preventDefault();
				var btnHref = "#";
        
				if ($(this).hasClass("left")){
					btnHref = "#site-navigation";
				}
				if ($(this).hasClass("right")){
					btnHref = "#site-sidebar";
				}
				openBar(btnHref, 400);
			});
  
			$(document).mouseup(function (e) {
				var container = $(".bar-opened");
				var btn = $(".bar-btn")
				
				if (!container.is(e.target)&& !btn.is(e.target)&& container.has(e.target).length === 0) { // if the target of the click isn't the container... nor a button controls the side bar... nor a descendant of the container
	
					container.each(function(){
						var x = closeBar(this, 400);
					});
				}
			});
			// Detect out of sidebar click. http://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it
		} else {

			$("html>body>#site>#main").css({"width": "", "float": ""});
			$("#watermark, #skip, #uw-header>.global-menu, #site-navigation, #uw-header #uw-search-label").show();
			
			var termOffset = $("#uw-header #uw-search-term").offset();
			$("#uw-header #uw-search-label").offset({top:termOffset.top+5,left:termOffset.left+5})

			$("#mob-menu-row, #mob-header").remove();
			$(".global-menu>li").css("float","");

			$("body").css("overflow-x","");
			$("#site-footer, #uw-footer").css("font-size", "");
			$("#site-navigation").show();
			$("#content").width("");
			
			$("#site-navigation, #site-sidebar").removeClass("mob-pan left-bar rightbar bar-closed bar-opened");	
			$("#site-navigation, #site-sidebar").css({"margin-top":"", "height":"", "left":"", "top":"", "width":"", "padding-top":"", "background-color":""});
			
			var contentWidth = 0;
			if ($("#site-sidebar").hasClass("no-content")) {
				//enlarge content because right sidebar has no content.
				contentWidth = $("#main").width()-70;
            } else {
				$("#site-sidebar").show();

				contentWidth = $("#main").width()-$("#site-sidebar")-20;
			}
		}	
		$("#content").width(contentWidth);
		

	} //end enableMobile()

	function openBar(ID, effectTime){
		if ($(ID).hasClass("no-content")){
		// Do not open it because the sidebar has no content.
		} else {
			if ($(ID).hasClass("sliding")){
			//Do nothing. Sliding in progresss
			} else {
				var nLeft = 0;
				if ($(ID).hasClass("bar-closed")){
					// Open the bar because it is closed; 
					if ($(ID).hasClass("left-bar")) {
						nLeft = 0;
					}
					if ($(ID).hasClass("right-bar")) {
						var barWidth = $(ID).width();
						var cWinSize = $(window).width();
						nLeft = cWinSize - barWidth-20;
					}
					//var cWinHeight = $("#site").height();
					//$(ID).height(cWinHeight);
					$(ID).addClass("sliding");
					$(ID).show(0, function(){
						$(ID).animate({left:nLeft}, effectTime, function(){
							$(ID).addClass("bar-opened");
							$(ID).removeClass("bar-closed sliding");
						});
					});
				}
			}
		}
	} // End openBar()

	function closeBar(ID, effectTime) {
		if ($(ID).hasClass("sliding")){
			//Do nothing. Sliding in progresss
		} else {
			var nLeft=0;
			if ($(ID).hasClass("bar-closed")){
			//Do nothing because the bar is already closed.
			} else {
        			
				if ($(ID).hasClass("left-bar")){
					var barWidth = $(ID).width();
					nLeft = -Math.abs(barWidth)-32;
				}
			
				if ($(ID).hasClass("right-bar")){
					var barWidth = $(ID).width();
					var cWinSize = $(window).width();
					nLeft = cWinSize + barWidth + 32;
				}
  				$(ID).addClass("sliding");
				$(ID).animate({left:nLeft}, effectTime, function(){
					$(ID).hide();
					$(ID).removeClass("sliding");
				});
			
				$(ID).addClass("bar-closed");
				$(ID).removeClass("bar-opened");
				return true;
			}
		}
	} // end closeBar()


})( jQuery );

