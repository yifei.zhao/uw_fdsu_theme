<?php

/**
 * Implements hook_preprocess_HOOK().
 *
 * Add custom color css file.
 */
function uw_fdsu_theme_resp_preprocess_html(&$variables) {
  // Add to body element a class to express the faculty or other organizational unit that this page is from.
  // This is used to select the faculty colors in CSS.
  $variables['classes_array'][] = 'org_' . variable_get('uw_fdsu_theme_color_css', 'default');

  // Add JavaScript for users with content authorship roles.
  global $user;
  if (array_intersect($user->roles, array('administrator', 'site manager', 'content editor', 'content author'))) {
    drupal_add_js(drupal_get_path('theme', 'uw_fdsu_theme') . '/scripts/hide-admin.js', 'file');
  }
  // add javascript for all users

 
  drupal_add_js(drupal_get_path('theme', 'uw_fdsu_theme') . '/scripts/expandable.js', 'file');

  //add class if this is a wide node -- method adapted from http://drupal.org/node/1072806
  // arg() function finds pages with internal Drupal path node/xx where xx is a number
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    //load the specific revision that is being displayed
    if (isset($variables['page']['content']['system_main']['nodes'][arg(1)]['body']['#object']->vid)) {
      $vid = $variables['page']['content']['system_main']['nodes'][arg(1)]['body']['#object']->vid;
      $node = node_load(arg(1),$vid);
      //if it's set to wide, add the "wide" class to the body
      if (isset($node->field_wide['und'][0]['value']) && $node->field_wide['und'][0]['value'] == 1) {
        $variables['classes_array'][] = 'wide';
      }
    }
  }

  //Add meta tags to enable mobile view.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' =>  'X-UA-Compatible',
      'content' => 'IE=edge',
    )
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' =>  'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0',
    )
  );
  
  // Add header meta tag to head
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
  
  
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function uw_fdsu_theme_resp_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  // remove the local site home link, and replace it with a link that uses the site name
  if (isset($breadcrumb[0])) {
    array_shift($breadcrumb);
    array_unshift($breadcrumb, l(variable_get('site_name', 'Site home'), '<front>'));
  }

  // Return the breadcrumb with separators.
  if (!empty($breadcrumb)) {
    $separator = ' » ';

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    return $heading . '<div class="breadcrumb">' . implode($separator, $breadcrumb) . $separator . '</div>';
  }
}

/**
 * Add theme suggestion for all content types
 */
function uw_fdsu_theme_resp_process_page(&$variables) {
  if (isset($variables['node'])) {
    if ($variables['node']->type != '') {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
    }
  }
}

/**
 * The h1 is part of the content in uw_ct_event. When it is not, generate it separately.
 */
function uw_fdsu_theme_resp_render_h1_and_content($page, $title, $site_name) {
  $output = '';
  $content = render($page['content']);
  if (strpos($content, '<h1') === FALSE) {
    $output .= '<h1>' . ($title ? $title : $site_name) . '</h1>';
  }
  $output .= $content;
  return $output;
}
